# Instagram-Firebase

A simplified version of Instagram using Firebase for user authentication, the database to store posts and storage to store media. As a user I can :-

* Sign up, sign in and sign out.
* Create new post (providing image and caption).
* View my posts.
* Delete posts.

Demo Video : https://www.dropbox.com/s/4sd88nc8s94mjcz/2-Instagram-Firebase.mp4?dl=0