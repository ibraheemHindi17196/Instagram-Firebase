//
//  Post.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/19/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import Foundation

class Post{
    var by = ""
    var image_url = ""
    var caption = ""
    var uid = ""
    var likes = 0
    
    init(){
        
    }
    
    init(by: String, image_url: String, caption: String) {
        self.by = by
        self.image_url = image_url
        self.caption = caption
    }
}
