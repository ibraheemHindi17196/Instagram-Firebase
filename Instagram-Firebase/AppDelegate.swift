//
//  AppDelegate.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/17/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
                
        // If a user is saved locally, skip login
        if let user_obj = UserDefaults.standard.value(forKey: "user") as? NSData {
            let user = NSKeyedUnarchiver.unarchiveObject(with: user_obj as Data) as! User
            
            let board : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let target_vc = board.instantiateViewController(withIdentifier: "tab_bar_vc") as! UITabBarController

            let feed_vc = target_vc.viewControllers![0] as! FeedViewController
            feed_vc.user = user

            let upload_vc = target_vc.viewControllers![1] as! UploadViewController
            upload_vc.user = user

            window?.rootViewController = target_vc
        }

        FirebaseApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

