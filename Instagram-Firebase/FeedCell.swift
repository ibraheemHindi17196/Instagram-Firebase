//
//  FeedCell.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/19/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import Kingfisher

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var post_by: UILabel!
    @IBOutlet weak var post_image: UIImageView!
    @IBOutlet weak var post_caption: UILabel!
    
    var post = Post()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
