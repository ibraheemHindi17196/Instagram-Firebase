//
//  LoginViewController.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/17/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import FirebaseAuth

class User: NSObject, NSCoding{
    var email: String
    var password: String
    
    override init() {
        self.email = ""
        self.password = ""
    }
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(email, forKey: "email")
        coder.encode(password, forKey: "password")
    }
    
    required convenience init?(coder decoder: NSCoder) {
        let email = decoder.decodeObject(forKey: "email") as! String
        let password = decoder.decodeObject(forKey: "password") as! String
        self.init(email: email, password: password)
    }
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username_field: UITextField!
    @IBOutlet weak var password_field: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var user = User()
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {
            UIAlertAction in
            
            if title == "Success"{
                self.createUser()
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(email_str: String) -> Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format:"SELF MATCHES %@", regex).evaluate(with: email_str)
    }
    
    func createUser(){
        self.user = User(
            email: self.username_field.text!,
            password: self.password_field.text!
        )
        
        self.storeUser(user: user)
        self.performSegue(withIdentifier: "toProfile", sender: nil)
    }
    
    func storeUser(user: User){
        let user_obj = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(user_obj, forKey: "user")
        UserDefaults.standard.synchronize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .crossDissolve
        
        if segue.identifier == "toProfile"{
            let destination = segue.destination as! UITabBarController
            
            let feed_vc = destination.viewControllers![0] as! FeedViewController
            feed_vc.user = user
            
            let upload_vc = destination.viewControllers![1] as! UploadViewController
            upload_vc.user = user
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func onUsernameEntered(_ sender: Any) {
        if password_field.text == ""{
            password_field.becomeFirstResponder()
        }
        else{
            resignFirstResponder()
        }
    }
    
    @IBAction func onPasswordEntered(_ sender: Any) {
        if username_field.text == ""{
            username_field.becomeFirstResponder()
        }
        else{
            resignFirstResponder()
        }
    }
    
    @IBAction func login(_ sender: Any) {
        if username_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide a username")
        }
        else if password_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide a password")
        }
        else{
            loader.startAnimating()
            Auth.auth().signIn(
                withEmail: username_field.text!,
                password: password_field.text!,
                completion: { (user, error) in
                    self.loader.stopAnimating()
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: "Login failed")
                    }
                    else{
                        self.createUser()
                    }
            })
        }
    }
    
    @IBAction func signup(_ sender: Any) {
        if username_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide a username")
        }
        else if !isValidEmail(email_str: username_field.text!){
            showSimpleAlert(title: "Error", message: "Please provide a valid email")
        }
        else if password_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide a password")
        }
        else{
            loader.startAnimating()
            Auth.auth().createUser(
                withEmail: username_field.text!,
                password: password_field.text!){(user, error) in
                    self.loader.stopAnimating()
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: (error?.localizedDescription)!)
                    }
                    else{
                        self.showSimpleAlert(title: "Success", message: "Successfully created your account")
                    }
            }
        }
    }

}











