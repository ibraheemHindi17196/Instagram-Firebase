//
//  FirstViewController.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/17/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import Kingfisher

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var no_posts_yet: UILabel!
    
    var user = User()
    var posts = [Post]()
    var cell_to_delete: FeedCell? = nil
    
    
    override func viewWillAppear(_ animated: Bool) {
        posts.removeAll()
        table_view.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchPosts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        table_view.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func confirmPostDeletion(){
        let alert = UIAlertController(
            title: "Confirm Deletion",
            message: "Are you sure you want to delete this post?",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default) {UIAlertAction in
            self.deletePost()
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePost(){
        loader.startAnimating()
        
        let ref = Database.database().reference()
            .child("users")
            .child((Auth.auth().currentUser?.uid)!)
            .child("post")
            .child((cell_to_delete?.post.uid)!)
        
        ref.removeValue{error,_ in
            if error != nil{
                self.showSimpleAlert(title: "Error", message: "An error occured. Please try again")
            }
            else{
                self.deletePostImage(image_url: (self.cell_to_delete?.post.image_url)!)
                self.fetchPosts()
            }
        }
    }
    
    func deletePostImage(image_url: String){
        let image_ref = Storage.storage().reference(forURL: image_url)
        
        image_ref.delete { error in
            if error != nil {
                self.showSimpleAlert(title: "Error", message: "An error occured. Please try again")
            }
            else {
                // Image deleted
            }
        }
    }
    
    func clearDatabase(){
        Database.database().reference().child("users").removeValue()
    }
    
    func fetchPosts(){
        posts.removeAll()
        table_view.reloadData()
        table_view.isHidden = true
        loader.startAnimating()
        var no_posts = false
        
        Database.database().reference().child("users").observe(.value) { (snapshot) in
            if snapshot.exists(){
                let value = snapshot.value as! NSDictionary
                let user_id = Auth.auth().currentUser?.uid
                
                if let user = value[user_id!]{
                    let user_posts = user as! NSDictionary
                    let post = user_posts["post"] as! NSDictionary
                    
                    for key in post.allKeys{
                        let post_obj = post[key] as! NSDictionary
                        
                        let post_entry = Post(
                            by: post_obj["by"]! as! String,
                            image_url: post_obj["image_url"]! as! String,
                            caption: post_obj["text"]! as! String
                        )
                        
                        post_entry.uid = key as! String
                        self.posts.append(post_entry)
                    }
                }
                else{
                    no_posts = true
                }
            }
            else{
                no_posts = true
            }
            
            self.loader.stopAnimating()
            
            if no_posts{
                self.table_view.isHidden = true
                self.no_posts_yet.alpha = 1
            }
            else{
                self.no_posts_yet.alpha = 0
                self.table_view.isHidden = false
                self.table_view.reloadData()
            }
        }
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    // On selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
        cell.post = posts[indexPath.row]
        cell.post_by.text = posts[indexPath.row].by
        cell.post_caption.text = posts[indexPath.row].caption
        
        // Set post image
        let url = URL(string: posts[indexPath.row].image_url)
        cell.post_image.kf.setImage(with: url)
        
        return cell
    }
    
    // Delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            cell_to_delete = self.table_view.cellForRow(at: indexPath)! as? FeedCell
            confirmPostDeletion()
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.synchronize()
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let board = UIStoryboard(name: "Main", bundle: nil)
        let login_vc = board.instantiateViewController(withIdentifier: "login_vc") as! LoginViewController
        app_delegate.window?.rootViewController = login_vc
    }

}

