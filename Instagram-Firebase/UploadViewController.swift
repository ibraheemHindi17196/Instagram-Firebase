//
//  SecondViewController.swift
//  Instagram-Firebase
//
//  Created by Ibraheem Hindi on 8/17/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class UploadViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var comment_field: UITextField!
    @IBOutlet weak var upload_button: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var user = User()
    var image = UIImage()
    var caption = ""
    var chosen_image = false
    var uuid = NSUUID().uuidString

    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func selectImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // After selecting an image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        image_view.image = image
        chosen_image = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadToFirebase(){
        loader.startAnimating()
        let media_folder = Storage.storage().reference().child("media")
        
        if let image_data = UIImageJPEGRepresentation(image, 0.5){
            let storage_ref = media_folder.child("\(uuid).jpg")
 
            storage_ref.putData(
                image_data,
                metadata: nil,
                completion: { (metadata, error) in
                    if error != nil{
                        self.showSimpleAlert(title: "Error", message: error.debugDescription)
                    }
                    else{
                        storage_ref.downloadURL { (url, error) in
                            if error != nil{
                                self.showSimpleAlert(title: "Error", message: error.debugDescription)
                            }
                            else{
                                let post = [
                                    "by" : Auth.auth().currentUser!.email!,
                                    "image_url" : url!.absoluteString,
                                    "text" : self.comment_field.text!,
                                    "likes" : 0
                                ] as [String : Any]
                                
                                let post_ref = Database.database().reference().child("users")
                                    .child((Auth.auth().currentUser?.uid)!)
                                    .child("post").childByAutoId()
                                
                                post_ref.setValue(post)
                                post_ref.observe(.childAdded, with: { (snapshot) in
                                    self.loader.stopAnimating()
                                    self.tabBarController?.selectedIndex = 0
                                    
                                    // Cleanup
                                    self.image = UIImage(named: "tap_to_select")!
                                    self.image_view.image = self.image
                                    self.comment_field.text = ""
                                })
                            }
                        }
                    }
                }
            )
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Adjust placeholder
        comment_field.attributedPlaceholder = NSAttributedString(
            string: "Caption",
            attributes: [
                .foregroundColor : UIColor.lightGray,
                .font : UIFont(name: "Avenir Next", size: 25)!
            ]
        )
        
        // Attach listener to image view
        image_view.isUserInteractionEnabled = true
        let tap_recognizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        image_view.addGestureRecognizer(tap_recognizer)
    }
    
    @IBAction func logout(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.synchronize()
        
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let board = UIStoryboard(name: "Main", bundle: nil)
        let login_vc = board.instantiateViewController(withIdentifier: "login_vc") as! LoginViewController
        app_delegate.window?.rootViewController = login_vc
    }
    
    @IBAction func onCommentEntered(_ sender: Any) {
        resignFirstResponder()
    }
    
    @IBAction func upload(_ sender: Any) {
        if comment_field.text == ""{
            showSimpleAlert(title: "Error", message: "Please provide a caption")
        }
        else if !chosen_image{
            showSimpleAlert(title: "Error", message: "Please select an image")
        }
        else{
            uploadToFirebase()
        }
    }

}

